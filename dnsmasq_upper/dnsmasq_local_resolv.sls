# Debian 9 light desktop - make dnsmasq be consulted by installing resolvconf
localres_plus:
  # Resolvconf and helpers
  pkg.installed:
  - pkgs:
    - resolvconf
    - dnsmasq-base   # Should already be installed in vm environment
    - dnsmasq        # Debian required
    - ldnsutils
  - install_recommends: false


officejava_purge:
  # Purge java related packages that may have come in with office suite
  # Specific to Debian.
  # Recommend just cherry pick 'localres_plus' or run test=True for this sls first before live run
  pkg.purged:
  - pkgs:
    - libreoffice-nlpsolver
    - libreoffice-report-builder
    - libreoffice-script-provider-bsh
    - libreoffice-sdbc-hsqldb
    - libreoffice-script-provider-js
    - default-jre
    - icedtea-netx
    - icedtea-8-plugin
    - default-java-plugin
    - openjdk-8-jre-headless
    - openjdk-8-jre
    - gcj-6-jre-lib
    - libgcj-common
    - libgcj17
    - libgcj-bc
    - libsac-java-gcj
    - java-common
    - ca-certificates-java
    - liblayout-java
    - libdom4j-java
    - libsaxonhe-java
    - libxmlbeans-java
  - require:
    - pkg: localres_plus
  - order: last

