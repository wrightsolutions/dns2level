# Pound as second layer behind dnsmasq
{% set domainmid = 'somedomain' %}
# Above we set the domain (middle) which you should override

und_plus:
  # Install Unbound and helpers
  pkg.installed:
    - pkgs:
      - unbound
      - dnsmasq-base   # Should already be installed in vm environment
      - dnsmasq        # Debian required
      - ldnsutils
    - install_recommends: false


default_dnsmasq:
  file.managed:
  - name: /etc/default/dnsmasq
  - contents: |
      # This file has five functions: 
      # 1) to completely disable starting dnsmasq, 
      # 2) to set DOMAIN_SUFFIX by running `dnsdomainname` 
      # 3) to select an alternative config file
      #    by setting DNSMASQ_OPTS to --conf-file=<file>
      # 4) to tell dnsmasq to read the files in /etc/dnsmasq.d for
      #    more configuration variables.
      # 5) to stop the resolvconf package from controlling dnsmasq's
      #    idea of which upstream nameservers to use.
      # For upgraders from very old versions, all the shell variables set 
      # here in previous versions are still honored by the init script
      # so if you just keep your old version of this file nothing will break.
      
      #DOMAIN_SUFFIX=`dnsdomainname`
      #DNSMASQ_OPTS="--conf-file=/etc/dnsmasq.alt"
      DNSMASQ_OPTS="--listen-address=127.0.0.1 --interface=lo"
      
      # Whether or not to run the dnsmasq daemon; set to 0 to disable.
      ENABLED=1
      
      # By default search this drop directory for configuration options.
      # Libvirt leaves a file here to make the system dnsmasq play nice.
      # Comment out this line if you don't want this. The dpkg-* are file
      # endings which cause dnsmasq to skip that file. This avoids pulling
      # in backups made by dpkg.
      #CONFIG_DIR=/etc/dnsmasq.d,.dpkg-dist,.dpkg-old,.dpkg-new
      CONFIG_DIR=/etc/dnsmasq.d,*.conf
      
      # If the resolvconf package is installed, dnsmasq will use its output 
      # rather than the contents of /etc/resolv.conf to find upstream 
      # nameservers. Uncommenting this line inhibits this behaviour.
      # Note that including a "resolv-file=<filename>" line in 
      # /etc/dnsmasq.conf is not enough to override resolvconf if it is
      # installed: the line below must be uncommented.
      #IGNORE_RESOLVCONF=yes
  - backup: minion


dnsmasq_local:
  file.managed:
  - name: /etc/dnsmasq.d/90-local_only.conf
  - contents: |
      # Listen on specified address only if net set already in /etc/default/dnsmasq
      listen-address=127.0.0.1
      interface=lo
      bind-interfaces
      # server dns directive to send matches to coded IP address
      server=/ccc.example.com/10.0.2.15
      # address dns directive for hardcoded translations
      address=/cdn.{{ domainmid }}.org/192.168.0.53
      address=/cdn-new.{{ domainmid }}.org/192.168.0.53
      address=/cdn2.{{ domainmid }}.org/192.168.0.53
      address=/cdn3.{{ domainmid }}.org/192.168.0.53
      address=/cdnnew.{{ domainmid }}.org/192.168.0.53
      address=/ccc.{{ domainmid }}.org/192.168.0.53

      #server=/ws.{{ domainmid }}.io/10.0.2.15


      # server dns directive to send matches to coded IP address
      #server=/ccc.example.com/10.0.2.15#1053
      #server=/cdn.{{ domainmid }}.org/10.0.2.15
      #server=/cdn-new.{{ domainmid }}.org/10.0.2.15
      #server=/cdn2.{{ domainmid }}.org/10.0.2.15
      #server=/cdn3.{{ domainmid }}.org/10.0.2.15
      #server=/cdnnew.{{ domainmid }}.org/10.0.2.15
      #server=/ccc.{{ domainmid }}.org/10.0.2.15
      
      #server=/ws.{{ domainmid }}.io/10.0.2.15

  #- order: last
  - backup: minion


dnsmasq_link_networkmanager:
  # Symlink in appropriate place pointing to /etc/dnsmasq.d/90-local_only
  file.symlink:
  - name: /etc/NetworkManager/dnsmasq.d/90-local_only
  - target: /etc/dnsmasq.d/90-local_only


unbound_local:
  file.managed:
  - name: /etc/unbound/unbound.conf.d/00-unbound_local.conf
  - contents: |
      # server section containing log directives and access control
      server:
              interface: 10.0.2.15@53
              logfile: /var/log/unbound.log
              verbosity: 3
              #log-queries: no
              log-queries: yes
              #access-control: 0.0.0.0/0 refuse
              access-control: 0.0.0.0/0 allow
              #local-zone: "cdn.{{ domainmid }}.org" redirect
              local-data: "cdn.{{ domainmid }}.org A 0.0.0.0"
              local-data: "cdn-new.{{ domainmid }}.org A 0.0.0.0"
              local-data: "cdn2.{{ domainmid }}.org A 0.0.0.0"
              local-data: "cdn3.{{ domainmid }}.org A 0.0.0.0"
              local-data: "cdnnew.{{ domainmid }}.org A 0.0.0.0"
              local-data: "ccc.{{ domainmid }}.org A 0.0.0.0"
  - backup: minion
  - require:
    - pkg: und_plus


unbound_checkconf:
  cmd.run:
  - name: 'unbound-checkconf /etc/unbound/unbound.conf'
  - require:
    - file: unbound_local
  - order: last

